package com.example.core.today;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.core.today.R.id;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.BaseAdapter;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;


public class CategoryItem extends Activity{
	String pager = null;
	CategoryAdapter gridViewCustomeAdapter;
	ArrayList<ArrayList<String>> arraylist;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.category_news);
		Intent receivedintent = getIntent();
		int get_id = receivedintent.getExtras().getInt("category");
		String category = null;
	
		if(get_id == R.id.ctg_economy)
			category = "경제";
		else if(get_id == R.id.ctg_entertain)
			category = "연예";
		else if(get_id == R.id.ctg_global)
			category = "세계";
		else if(get_id == R.id.ctg_it)
			category = "IT";
		else if(get_id == R.id.ctg_main)
			category = "주요뉴스";
		else if(get_id == R.id.ctg_politics)
			category = "정치사회";
		else if(get_id == R.id.ctg_sports)
			category = "스포츠";
		GridView gridView = (GridView)findViewById(R.id.gridView);
		TextView text = (TextView)findViewById(R.id.ctg);
		text.setText(category);
		Button btn = (Button)findViewById(R.id.finish);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		Toast.makeText(getBaseContext(), "complete 라능", 5000).show();
		
		pager= CategoryMaker();
		gridViewCustomeAdapter = new CategoryAdapter(this, get_id, pager);
		gridView.setAdapter(gridViewCustomeAdapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
		
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				try {
					JSONArray  ForUrl = null;
					JSONObject ForUrl2= null;
					ForUrl = new JSONArray(pager);
					ForUrl2= new JSONObject(ForUrl.getString(position));
					String sub = ForUrl2.getString("sub");
					ForUrl = new JSONArray(sub);
					ForUrl2 = new JSONObject(ForUrl.getString(0));
					
					String url = ForUrl2.getString("url");
					Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
					startActivity(intent);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Toast.makeText(getBaseContext(), e.toString(), 5000).show();
				}
				
				
			}
			
		});

	}
	

	private String CategoryMaker(){
   	String key = "";
   	String news = "";
   	String url_info = "";
    String line;
    String page ="";
	String group1 = "http://kr.core.today/json/?d=20141108&n=10";
    String sample = group1; 

    ArrayList<String> title = null;
    ArrayList<String> index = null;
        URL url;
        HttpURLConnection urlConnection;
        BufferedReader bufreader;
        try {
		url = new URL(sample);		
		urlConnection = (HttpURLConnection) url.openConnection();
		bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
		while((line=bufreader.readLine())!=null){
			page+=line;
		}

		urlConnection.disconnect();
		
		}
		catch(Exception e){

		}

        return page;
    }
    
}
