package com.example.core.today;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class EventlineNews extends Activity{
	private ListView mEventListView;
	private EventlineNewsAdapter mEventAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eventline_news);
		mEventAdapter = new EventlineNewsAdapter();
		mEventListView = (ListView)findViewById(R.id.eventline_listview);
		mEventListView.setAdapter(mEventAdapter);
		mEventListView.setOnItemClickListener(onClickListItem);
		
	}
	
	private OnItemClickListener onClickListItem = new OnItemClickListener(){
		
		@Override
		public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
			
		}
		
	};
	
}
