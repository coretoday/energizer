package com.example.core.today;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

public class FullNews extends Activity{

	private GridView                mGridView;
	private ArrayAdapter<Bitmap>    mAdapterPic;
    private FullNewsAdapter    mAdapterText;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_news);
         
      
        mAdapterText = new FullNewsAdapter();
         
      
        mGridView = (GridView) findViewById(R.id.gridView1);
         
       
        mGridView.setAdapter(mAdapterText);
         
      
        mGridView.setOnItemClickListener(onClickListItem);
         
      
    }
    
    private OnItemClickListener onClickListItem = new OnItemClickListener() {
    	 
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            
            
        }

		
    };
}
