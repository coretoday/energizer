package com.example.core.today;

import viewpagerindicator.TabPageIndicator;
import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.internal.view.menu.MenuView.ItemView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshListView;


public class MainActivity extends FragmentActivity implements OnClickListener, OnQueryTextListener  {
	private static final String[] CONTENT = new String[] { "시사", "스포츠", "연예" };

	boolean isPageOpen = false;
	Animation translateLeftAnim;
	Animation translateRightAnim;
	LinearLayout leftmenu;
	LinearLayout frontpage;
	LinearLayout empty;
	FrameLayout main;
	//ImageView menu_btn;
	Button login_btn;
	ImageButton sports;
	ImageButton politics;
	ImageButton economy;
	ImageButton it;
	ImageButton ranking;
	ImageButton entertain;
	ImageButton global;
	static final int LOGIN = 0;
	static final int PROFILE = 1;
	static final int MENU_SET_MODE = 2;
	static final int MENU_DEMO = 3;

	private PullToRefreshListView mPullRefreshListView;
	
	private final boolean IS_ICON_ITEM = true;
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_ptr_list_in_vp);	
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		//getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME );
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
	    TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
	    Typeface font1 = Typeface.createFromAsset(getAssets(), "THEjunggt120.otf");
	    actionBarTitleView.setTypeface(font1);
	    getActionBar().setTitle("PRISM");
		////////////////////////////////////////////////////////////////////////////////
		FragmentPagerAdapter adapter = new ListViewPagerAdapter(getSupportFragmentManager());
		mViewPager = (ViewPager) findViewById(R.id.vp_list);
		mViewPager.setAdapter(adapter);
		mViewPager.setOffscreenPageLimit(2);
		TabPageIndicator indicator = (TabPageIndicator)findViewById(R.id.indicator);
	    indicator.setViewPager(mViewPager);
	    ////////////////////////////////////////////////////////////////////////////////activate fragment
	    
	    
	    //menu_btn = (ImageView)findViewById(R.id.menu_btn);  
	    empty=(LinearLayout) findViewById(R.id.empty);
	    frontpage=(LinearLayout)findViewById(R.id.abc);
	    leftmenu=(LinearLayout)findViewById(R.id.leftmenu);
	    login_btn= (Button)findViewById(R.id.login_total);
	    
	    
	    leftmenu.setOnClickListener(this);
	    frontpage.setOnClickListener(this);
	    empty.setOnClickListener(this);  
	    //menu_btn.setOnClickListener(this);
	    login_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent login_intent = null;
				login_intent = new Intent(MainActivity.this, Login.class);
				startActivity(login_intent);
				
			}
		});
	    
	    /////////////////////////////////////////////////////////////////////////////////////
	    translateLeftAnim = AnimationUtils.loadAnimation(this, R.anim.translate_left);
	    translateRightAnim = AnimationUtils.loadAnimation(this, R.anim.translate_right);
	    SlidingPageAnimationListener animListener = new SlidingPageAnimationListener();
	    translateLeftAnim.setAnimationListener(animListener);
	    translateRightAnim.setAnimationListener(animListener);
	    ////////////////////////////////////////////////////////////////////////////////leftmenu Animation
	    
	   
//	    TextView timeline_btn = (TextView)findViewById(R.id.timeline);
	    sports   =(ImageButton)findViewById(R.id.ctg_sports);
	    politics =(ImageButton)findViewById(R.id.ctg_politics);
	    ranking  =(ImageButton)findViewById(R.id.ctg_main);
	    entertain=(ImageButton)findViewById(R.id.ctg_entertain);
	    it=(ImageButton)findViewById(R.id.ctg_it);
	    economy  =(ImageButton)findViewById(R.id.ctg_economy);
	    global = (ImageButton)findViewById(R.id.ctg_global);
	    
	    global.setOnClickListener(new OnClickListener(){

		    @Override
		    public void onClick(View v) {
		    // TODO Auto-generated method stub

		    Intent intent = new Intent(getBaseContext(), CategoryItem.class);
		    intent.putExtra("category", R.id.ctg_global);
		    startActivity(intent);

		 }
	    });

	    sports.setOnClickListener(new OnClickListener(){

	    @Override
	    public void onClick(View v) {
	    // TODO Auto-generated method stub

	    Intent intent = new Intent(getBaseContext(), CategoryItem.class);
	    intent.putExtra("category", R.id.ctg_sports);
	    startActivity(intent);

	    }

	    });
	    politics.setOnClickListener(new OnClickListener(){

	    @Override
	    public void onClick(View v) {
	    // TODO Auto-generated method stub
	    Intent intent = new Intent(getBaseContext(), CategoryItem.class);
	    intent.putExtra("category", R.id.ctg_politics);
	    startActivity(intent);
	    }

	    });
	    ranking.setOnClickListener(new OnClickListener(){

	    @Override
	    public void onClick(View v) {
	    // TODO Auto-generated method stub
	    Intent intent = new Intent(getBaseContext(), CategoryItem.class);
	    intent.putExtra("category", R.id.ctg_main);
	    startActivity(intent);
	    }

	    });
	    entertain.setOnClickListener(new OnClickListener(){

	    @Override
	    public void onClick(View v) {
	    // TODO Auto-generated method stub
	    Intent intent = new Intent(getBaseContext(), CategoryItem.class);
	    intent.putExtra("category", R.id.ctg_entertain);
	    startActivity(intent);
	    }

	    });
	    it.setOnClickListener(new OnClickListener(){

	    @Override
	    public void onClick(View v) {
	    // TODO Auto-generated method stub
	    Intent intent = new Intent(getBaseContext(), CategoryItem.class);
	    intent.putExtra("category", R.id.ctg_it);
	    startActivity(intent);
	    }

	    });
	    economy.setOnClickListener(new OnClickListener(){

	    @Override
	    public void onClick(View v) {
	    // TODO Auto-generated method stub
	    Intent intent = new Intent(MainActivity.this, CategoryItem.class);
	    intent.putExtra("category", R.id.ctg_economy);
	    startActivity(intent);
	    }

	    });
	    ImageButton timeline_btn = (ImageButton)findViewById(R.id.timeline);
	 
	    timeline_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = null;
				intent = new Intent(MainActivity.this, TimeLine.class);
				startActivity(intent);
				
			}
		});
	    

	}
    private class SlidingPageAnimationListener implements AnimationListener {
    	/**
    	 * 좌측 메뉴 애니메이션이 끝날 때 호출되는 메소드
    	 */
		public void onAnimationEnd(Animation animation) {
			if(isPageOpen)
			{
				
				frontpage.setClickable(true);
			}
			else
			{
				
				frontpage.setClickable(false);
			}
		}

		public void onAnimationRepeat(Animation animation) {
			

		}

		public void onAnimationStart(Animation animation) {
			if(isPageOpen)
			{
				leftmenu.setVisibility(View.INVISIBLE);
				empty.setVisibility(View.INVISIBLE);
				isPageOpen=false;
			}
			else
			{
				isPageOpen=true;
				empty.setVisibility(View.VISIBLE);
			}
		}

    }

	private class ListViewPagerAdapter extends FragmentPagerAdapter {

		public ListViewPagerAdapter(FragmentManager fm) {
			super(fm);
			// TODO Auto-generated constructor stub
		}
		
        @Override
        public Fragment getItem(int position) {
        	switch(position){
        	 case 0: 
        		 return TestFragment.newInstance("20141101");
             case 1:
                 return TestFragment.newInstance("20141103");
             case 2:
                 return TestFragment.newInstance("20141108");
             default:
            	 return null;
        	}
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }



	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem searchMenu = menu.findItem(R.id.menu_search);
		
		
		SearchView searchView = (SearchView)searchMenu.getActionView();
		
		searchView.setOnQueryTextListener(this);
		searchView.setSubmitButtonEnabled(true);
		
		searchView.setQueryHint("키워드를 입력해보세요");
		
		if(IS_ICON_ITEM){
			searchMenu.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		}else{
			searchMenu.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			searchView.setIconifiedByDefault(false);
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText){
		return false;
	}
	
	@Override
	public boolean onQueryTextSubmit(String query){
		return false;
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;

		menu.add("Item 1");
		menu.add("Item 2");
		menu.add("Item 3");
		menu.add("Item 4");

		super.onCreateContextMenu(menu, v, menuInfo);
	}


	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
			break;
		
		case android.R.id.home:
			
			if(isPageOpen == false){
				leftmenu.setVisibility(View.VISIBLE);
				leftmenu.startAnimation(translateRightAnim);
			}
			else if(isPageOpen){
				leftmenu.startAnimation(translateLeftAnim);
			}
			
			break;
		}
		return super.onOptionsItemSelected(item);
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
		////////////////////////////////////////////////////////////
		if(v.getId()==android.R.id.home)
		{
			leftmenu.setVisibility(View.VISIBLE);
			leftmenu.startAnimation(translateRightAnim);
		}
		else if(isPageOpen)
		{
			if(v.getId()!=R.id.leftmenu)
			{
				leftmenu.startAnimation(translateLeftAnim);
			}
		}
		///////////////////////////////////////////////////////////leftmenu animation
		
		
	}
	
	


	
}
