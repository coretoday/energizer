package com.example.core.today;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CategoryAdapter extends ArrayAdapter{
	ArrayList<String> title = new ArrayList<String>();
	ArrayList<String> index = new ArrayList<String>();
	String jsonfile;
	ArrayList<ArrayList<String>> arraylist = new ArrayList<ArrayList<String>>();
	
	public CategoryAdapter(Context context, int resource, String json) {
		super(context, resource);
		mcontext = context;
		id = resource;
		jsonfile = json;
		arraylist.add(title);
		arraylist.add(index);

	}
	int id;
	Context mcontext;
	
	
	public int getCount(){
		return 10;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		
		if(row==null && position <= getCount()){
			LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(R.layout.grid_row, parent, false);
                        
           TextView textViewTitle = (TextView) row.findViewById(R.id.title_text);
           ImageView newsimage = (ImageView) row.findViewById(R.id.image);
         
           
           JSONObject test = null;
           
           try {
        	   
			JSONArray alljson = new JSONArray(jsonfile);
			test = new JSONObject(alljson.getString(position));
			URL articleURL = null;
			articleURL = new URL(test.getString("i"));
			
			HttpURLConnection conn = (HttpURLConnection)articleURL.openConnection();   
            conn.setDoInput(true);   
            conn.connect();   
			int length = conn.getContentLength();
			InputStream is = conn.getInputStream(); 
			Bitmap bmImg; 
			bmImg = BitmapFactory.decodeStream(is);
           
        	bmImg = resizeBitmapImageFn(bmImg);
        	conn.disconnect();

			textViewTitle.setText(test.getString("k"));
			newsimage.setImageBitmap(bmImg);
           } 
           catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
           }
                
		}
		
	
		return row;
	}
	public Bitmap resizeBitmapImageFn(
    		Bitmap bmpSource){    		        
    		        return Bitmap.createScaledBitmap(
    		bmpSource, 50, 50, true); 
    		}
}
